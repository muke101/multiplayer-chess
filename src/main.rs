#![allow(dead_code)]

use std::collections::HashMap;
use std::io::{stdin,stdout,Write};
use std::error::Error;

#[derive(Clone,Copy,PartialEq,Eq)]
enum Colour {
    White,
    Black,
}
 
#[derive(Clone,Copy,Hash,PartialEq,Eq)]
enum Unit  {
    Pawn,
    Rook,
    Knight,
    Bishop,
    King,
    Queen,
}

enum Direction  {
    Up,
    Down,
    Left,
    Right,
    UpLeft,
    UpRight,
    DownLeft,
    DownRight,
}
 
#[derive(Clone,Copy)]
struct Piece    {
    colour: Colour,
    unit: Unit,
}

#[derive(Clone,Copy)]
struct Point  {
    x: i32,
    y: i32,
}
 
#[derive(Clone,Copy)]
struct Square  {
    dis: Colour,
    occupied: Option<Piece>, 
    pos: Point, 
}

impl Square {
    fn new(x: i32, y: i32, display: Colour) -> Square  {
        let Point = Point {x: x, y: y};
        Square  {
            dis: display,
            occupied: None,
            pos: Point,
        }
    }

    fn next(&self, direction: Direction) -> Option<Point>  {
        match direction {
            Direction::Up => if self.pos.y != 0 { Some(Point {x: self.pos.x, y: self.pos.y+1}) } else { None },
            Direction::Down => if self.pos.y != 7 { Some(Point {x: self.pos.x, y: self.pos.y-1}) } else { None },
            Direction::Left => if self.pos.x != 0 { Some(Point {x: self.pos.x-1, y: self.pos.y}) } else { None },
            Direction::Right => if self.pos.x != 7 { Some(Point {x: self.pos.x+1, y: self.pos.y}) } else { None },
            Direction::UpLeft => if self.pos.x != 0 || self.pos.y != 0 { Some(Point {x: self.pos.x-1, y: self.pos.y+1}) } else { None },
            Direction::UpRight => if self.pos.x != 7 || self.pos.y != 0 { Some(Point {x: self.pos.x+1, y: self.pos.y+1}) } else { None },
            Direction::DownLeft => if self.pos.x != 0 || self.pos.y != 7 { Some(Point {x: self.pos.x-1, y: self.pos.y-1}) } else { None },
            Direction::DownRight => if self.pos.x != 7 || self.pos.y != 7 { Some(Point {x: self.pos.x+1, y: self.pos.y-1}) } else { None },
        }
    }
}
 
type Board = [[Square; 8]; 8];

fn set_pieces(board: &mut Board, col: Colour, rows: (usize, usize)) {

    for i in 0..8   {
        board[rows.0][i].occupied = Some(Piece {colour: col, unit: Unit::Pawn});
    }

    board[rows.1][0].occupied = Some(Piece {colour: col, unit: Unit::Rook});
    board[rows.1][7].occupied = Some(Piece {colour: col, unit: Unit::Rook});
    board[rows.1][1].occupied = Some(Piece {colour: col, unit: Unit::Knight});
    board[rows.1][6].occupied = Some(Piece {colour: col, unit: Unit::Knight});
    board[rows.1][2].occupied = Some(Piece {colour: col, unit: Unit::Bishop});
    board[rows.1][5].occupied = Some(Piece {colour: col, unit: Unit::Bishop});
    board[rows.1][3].occupied = Some(Piece {colour: col, unit: Unit::Queen});
    board[rows.1][4].occupied = Some(Piece {colour: col, unit: Unit::King});

}
 
fn set_board(board: &mut Board, side: &str)   {

    for i in 0..8   {
        for j in 0..8   {
            let dis: Colour; 
            if (j+i) % 2 == 0 { dis = Colour::White; } else { dis = Colour::Black; }
            board[i][j] = Square::new(j as i32, i as i32, dis);
        }
    }

    if side == "white"  {
        set_pieces(board, Colour::White, (5,6));
        set_pieces(board, Colour::Black, (1,0));
    }

    if side == "black"  {
        set_pieces(board, Colour::Black, (5,6));
        set_pieces(board, Colour::White, (1,0));
    }
}

fn print_board(board: &mut Board)   {

    let mut white_pieces: HashMap<Unit, &str> = HashMap::new();
    white_pieces.insert(Unit::Pawn, "♟︎" );
    white_pieces.insert(Unit::Rook, "♜" );
    white_pieces.insert(Unit::Knight, "♞");
    white_pieces.insert(Unit::Bishop, "♝");
    white_pieces.insert(Unit::King, "♚");
    white_pieces.insert(Unit::Queen, "♛");

    let mut black_pieces: HashMap<Unit, &str> = HashMap::new();
    black_pieces.insert(Unit::Pawn, "♙");
    black_pieces.insert(Unit::Rook, "♖");
    black_pieces.insert(Unit::Knight, "♘");
    black_pieces.insert(Unit::Bishop, "♗");
    black_pieces.insert(Unit::King, "♔");
    black_pieces.insert(Unit::Queen, "♕");

    for _i in 0..9   {
        print!(" = ");
    }
    println!();

    for i in 0..7   {
        print!(" | ");
        for j in 0..7   {
            match board[i][j].occupied   {
                Some(piece) => match piece.colour    {
                    Colour::White => print!(" {} ", white_pieces.get(&piece.unit).unwrap()),
                    Colour::Black => print!(" {} ", black_pieces.get(&piece.unit).unwrap()),
                }
                None => match board[i][j].dis   {
                    Colour::White => print!(" x "),
                    Colour::Black => print!(" - "),
                }
            }
        }
        println!(" | ");
    }

    for _i in 0..9   {
        print!(" = ");
    }
    println!();
}


fn index<'a>(board: &'a Board, point: Point) -> &'a mut Square    {
    return &mut board[point.y as usize][point.x as usize];
}

fn mov_rook(board: &Board, start: Point, end: Point) -> Result<(), Box<dyn Error>>  {

    let grad = i32::abs((start.y - end.y)/(start.x - end.x));

    if grad != 0    { 
        return Ok(());
    }


}

fn mov(board: &mut Board, start: Point, end: Point) -> Result<(), Box<dyn Error>>   {

    match index(board, start).occupied   {
        Some(piece) => match piece.unit    {
            Unit::Rook => mov_rook(board, start, end),
            _ => Ok(())
        }
        None => Ok(())
    } 

}
 
fn main() -> Result<(), Box<dyn Error>> {
    let initial_square = Square::new(0,0,Colour::White); //placeholder as rust sucks at 2D arrays apparently
    let mut board: Board = [[initial_square; 8]; 8];

    let mut side = String::new();

    print!("Whites or Blacks: ");
    stdout().flush()?;
    stdin().read_line(&mut side)?; 
    
    side = side.trim().to_lowercase();

    if side == "white" || side == "whites"  {
        set_board(&mut board, "white");
    }
    else if side == "black" || side == "blacks"  {
        set_board(&mut board, "black");
    }
    else    {
        println!("Invalid side, please choose either 'Whites' or 'Blacks'");
        return Ok(());
    }

    println!();
    print_board(&mut board);
    println!();

    Ok(())
}

/* create types for white and black peices and then a generic function that pattern matches on the type to determine movement
 * can use these generic types abstractly the program same behavior regardless of color, for
 * instance in determining friend of foe in a take
 * graph could be a matrix of structs with associated types - white square, black square, edge and
 * then vertical/horizontal, each struct contains what's on it and pointers to their neighbors, so
 * a linked list localized to a grid array
 * then have functions that corrpond to each type of piece that you match against the type of to
 * call
 */

